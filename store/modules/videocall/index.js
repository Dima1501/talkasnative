import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
  callerCandidates: [],
  calleeCandidates: [],
  videocalls: {},
  pc: null,
  activeCall: null
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
};