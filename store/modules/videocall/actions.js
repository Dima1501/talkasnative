import Vue from 'vue'
import {db} from '../../../main'
import firebase from 'firebase/app'
require('firebase/auth')
require('firebase/database')
import 'firebase/firestore'

const stopVideoCall = (store, payload) => {

  store.commit('removeCall', payload)
  store.commit('view/disableVideoCall', {}, {root: true})

  firebase.firestore().collection('users').where("uid", "==", store.rootState.auth.user.uid).get().then(function(querySnapshot) {
    querySnapshot.forEach(function(doc) {
      firebase.firestore().collection("users").doc(doc.ref.id).update({
        status: {
          online: true,
          videoCall: false
        }
      })
    })
  })

  firebase.firestore().collection('videocalls').doc(payload.from).set({
    call: false,
    to: payload.to,
    from: payload.from
  })

  let localVideo = document.getElementById('localVideo'),
      remoteVideo = document.getElementById('remoteVideo')
      
  if (store.state.pc) {
    store.state.pc.ontrack = null
    store.state.pc.onremovetrack = null
    store.state.pc.onremovestream = null
    store.state.pc.onicecandidate = null
    store.state.pc.oniceconnectionstatechange = null
    store.state.pc.onsignalingstatechange = null
    store.state.pc.onicegatheringstatechange = null
    store.state.pc.onnegotiationneeded = null

    if (remoteVideo.srcObject) {
      remoteVideo.srcObject.getTracks().forEach(track => track.stop())
    }

    if (localVideo.srcObject) {
      localVideo.srcObject.getTracks().forEach(track => track.stop())
    }

    store.state.pc.close()
    store.state.pc = null
  }

  remoteVideo.removeAttribute("src")
  remoteVideo.removeAttribute("srcObject")
  localVideo.removeAttribute("src")
  remoteVideo.removeAttribute("srcObject")
  console.log('stop')
}

const waitVideoCall = (store) => {

  firebase.firestore().collection('videocalls').where('from', '==', store.rootState.auth.user.uid).onSnapshot((snapshot) => {
    snapshot.forEach(doc => {
      if (doc.data().call == false) {
        if (store.state.activeCall == doc.data().to) {
          store.dispatch('stopVideoCall', doc.data())
        }
      }
    })
  })

  firebase.firestore().collection('videocalls').where('to', '==', store.rootState.auth.user.uid).onSnapshot((snapshot) => {
    snapshot.forEach(doc => {

      if (doc.data().call == false) {
        if (store.state.activeCall == doc.data().from) {
          store.dispatch('stopVideoCall', doc.data())
        }

        let payload = {
          [doc.data().from]: {
            call: false,
            from: doc.data().from,
            to: doc.data().to
          }
        }
  
        Vue.set(store.state.videocalls, [doc.data().from], payload[doc.data().from])

      } else if (doc.data().call == true) {
        
        let payload = {
          [doc.data().from]: {
            offer: doc.data().offer,
            callerCandidates: doc.data().callerCandidates,
            from: doc.data().from,
            to: doc.data().to,
            call: true,
            fromName: doc.data().fromName.userName,
            fromImage: doc.data().fromName.userImage
          }
        }
        store.state.activeCall = doc.data().from
        console.log('______')
        console.log(store.state.activeCall)
  
        Vue.set(store.state.videocalls, [doc.data().from], payload[doc.data().from])
        
      }
    })
  })
}

const applyCall = (store, call) => {

  require('webrtc-adapter')

  let localVideo = document.getElementById('localVideo'),
      remoteVideo = document.getElementById('remoteVideo')
  
  let configuration = {
    iceServers: [
        {urls: "stun:23.21.150.121"},
        {urls: "stun:stun.l.google.com:19302"},
        {urls: "turn:numb.viagenie.ca", credential: "webrtcdemo", username: "louis%40mozilla.com"}
    ]
  }

  let constraints = {
    video: true,
    audio: true
  }

  firebase.firestore().collection('users').where("uid", "==", store.rootState.auth.user.uid).get().then(function(querySnapshot) {
    querySnapshot.forEach(function(doc) {
      firebase.firestore().collection("users").doc(doc.ref.id).update({
        status: {
          online: true,
          videoCall: true
        }
      })
    })
  })

  store.state.pc = new RTCPeerConnection(configuration)

  store.state.pc.onconnectionstatechange = function(event) {
    switch(store.state.pc.connectionState) {
      case "connected":
        console.log('connected')
        break
      case "disconnected":
        console.log('disconnected')

        firebase.firestore().collection('videocalls').where('from', '==', store.rootState.auth.user.uid).get().then((snapshot) => {
          snapshot.forEach(doc => {
              store.dispatch('stopVideoCall', doc.data())
              store.commit('removeCall', doc.data())
          })
        })

        firebase.firestore().collection('videocalls').where('to', '==', store.rootState.auth.user.uid).get().then((snapshot) => {
          snapshot.forEach(doc => {
              store.dispatch('stopVideoCall', doc.data())
              store.commit('removeCall', doc.data())
          })
        })

        break
      case "failed":
          console.log('failed')
        break
      case "closed":
        console.log('closed')
        break
    }
  }

  store.state.pc.onicecandidate = (event) => {
    if (event.candidate) {
      store.commit('addCalleeCandidate', event.candidate.toJSON())
      firebase.firestore().collection('videocalls').doc(call.from).update({
        calleeCandidates: store.state.calleeCandidates
      })
    } else {
      console.log('All candidates found')
    }
  }

  firebase.firestore().collection('videocalls').doc(call.from).onSnapshot((snapshot) => {
    if (snapshot.data().callerCandidates !== undefined && snapshot.data().callerCandidates.length > 0) {
      for (let candidate of snapshot.data().callerCandidates) {
        console.log(candidate)
        console.log(store)
        store.state.pc.addIceCandidate(candidate)
          .catch(err => console.log(err))
      }
    }
  })

  store.state.pc.ontrack = (e) => {
    remoteVideo.srcObject = e.streams[0]
    remoteVideo.play()
  }

  store.state.pc.setRemoteDescription(call.offer)
    .then(() => {
      navigator.mediaDevices.getUserMedia(constraints)
    .then((stream) => {
      stream.getTracks().forEach((track) => {
        store.state.pc.addTrack(track, stream)
        localVideo.srcObject = stream
      })
      store.commit('view/toggleVideoCall', {}, {root: true})
    }).then(() => {
      store.state.pc.createAnswer()
        .then((answer) => {
          store.state.pc.setLocalDescription(answer)
            .then(() => {
              firebase.firestore().collection('videocalls').doc(call.from).update({
                answer: answer.toJSON ? answer.toJSON() : answer
              })
            })
        })
      })
    })
      
}

const call = (store, payload) => {

  require('webrtc-adapter')

  let calleeRef = firebase.firestore().collection('users').where("uid", "==", payload.userId),
      roomRef = firebase.firestore().collection('conversations').doc(payload.roomId),
      callerRef = firebase.firestore().collection('users').where("uid", "==", store.rootState.auth.user.uid)

  let calleeData,
      roomData

  let localVideo = document.getElementById('localVideo'),
      remoteVideo = document.getElementById('remoteVideo')

  let configuration = {
    iceServers: [
        {urls: "stun:23.21.150.121"},
        {urls: "stun:stun.l.google.com:19302"},
        {urls: "turn:numb.viagenie.ca", credential: "webrtcdemo", username: "louis%40mozilla.com"}
    ]
  }

  let constraints = { 
    video: true,
    audio: true
  }

  store.state.activeCall = payload.userId

  store.state.pc = new RTCPeerConnection(configuration)

  store.state.pc.onconnectionstatechange = function(event) {
    switch(store.state.pc.connectionState) {
      case "connected":
        console.log('connected')
        break
      case "disconnected":
        console.log('disconnected')

        firebase.firestore().collection('videocalls').where('from', '==', store.rootState.auth.user.uid).get().then((snapshot) => {
          snapshot.forEach(doc => {
            store.dispatch('stopVideoCall', doc.data())
            store.commit('removeCall', doc.data())
          })
        })

        firebase.firestore().collection('videocalls').where('to', '==', store.rootState.auth.user.uid).get().then((snapshot) => {
          snapshot.forEach(doc => {
            store.dispatch('stopVideoCall', doc.data())
            store.commit('removeCall', doc.data())
          })
        })

        break
      case "failed":
          console.log('failed')
        break
      case "closed":
        console.log('closed')
        break
    }
  }

  firebase.firestore().collection('videocalls').doc(store.rootState.auth.user.uid).onSnapshot((snapshot) => {
    if (snapshot.data() && snapshot.data().calleeCandidates !== undefined) {
      for (let candidate of snapshot.data().calleeCandidates) {
        store.state.pc.addIceCandidate(candidate)
          .catch(err => console.log(err))
      }
    }
  })

  store.state.pc.onicecandidate = (event) => {
    if (event.candidate) {
      store.commit('addCandidate', event.candidate.toJSON())
      firebase.firestore().collection('videocalls').doc(store.rootState.auth.user.uid).update({
        callerCandidates: store.state.callerCandidates
      })
    } else {
      console.log('All candidates found')
    }
  }

  store.state.pc.ontrack = (e) => {
    remoteVideo.srcObject = e.streams[0]
    remoteVideo.play()
  }

  firebase.database().ref('users/' + payload.userId).once('value').then(snapshot => {

    if (snapshot.val() == 'online') {
      calleeRef.get().then(snapshot => {
        snapshot.forEach(doc => {
          calleeData = doc.data()
        })
      }).then(() => {
        if (!calleeData.status.videoCall) {
          startVideoCall()
        } else {
          store.dispatch('view/showNotify', 'The user talks now, please try later', {root:true})
        }
      })
    } else {
      store.dispatch('view/showNotify', 'The user offline now, please try later', {root:true})
    }
  })

  
  const startVideoCall = () => {

    firebase.firestore().collection('videocalls').doc(store.rootState.auth.user.uid).set({}).catch(err => console.log(err))

    callerRef.get().then(function(querySnapshot) {
      querySnapshot.forEach(function(doc) {
        firebase.firestore().collection("users").doc(doc.ref.id).update({
          status: {
            online: true,
            videoCall: true
          }
        })
      })
    })

    navigator.mediaDevices.getUserMedia(constraints)
    .then((stream) => {
      stream.getTracks().forEach((track) => {
        store.state.pc.addTrack(track, stream)
        localVideo.srcObject = stream
      })
    }).then(() => {

      let videoCallInfo = {
        to: payload.userId,
        from: store.rootState.auth.user.uid,
        fromName: store.rootState.auth.user.userName,
        fromImage: store.rootState.auth.user.userImage
      }

      store.commit('view/toggleVideoCall', {}, {root: true})
      store.commit('addCall', videoCallInfo)

      store.state.pc.createOffer().then(offer => {
        store.state.pc.setLocalDescription(offer).then(() => {
          firebase.firestore().collection('videocalls').doc(store.rootState.auth.user.uid).set({
            offer: offer.toJSON ? offer.toJSON() : offer,
            to: payload.userId,
            from: store.rootState.auth.user.uid,
            call: true,
            fromName: store.rootState.auth.userData
          })
          .then(() => {
            firebase.firestore().collection('videocalls').doc(store.rootState.auth.user.uid).onSnapshot((snapshot) => {
              if (snapshot.data() !== undefined && snapshot.data().answer !== undefined) {
                store.state.pc.setRemoteDescription(snapshot.data().answer)
                  .catch(err => console.log(err))
              }
            })
          })
          .catch(err => console.log(err))
        })
      })
    }).catch((err) => {
      console.log(err)
      store.dispatch('view/showNotify', 'We need access to media devices, reload page to try again', {root:true})
    })
  }

  
}

export default {
  call,
  applyCall,
  waitVideoCall,
  stopVideoCall
}