const showPopup = (store, type) => {
  store.commit('setPopupState', type)
}

const showNotify = (store, message) => {
  store.commit('toggleNotify', message)

  let timeout = setTimeout(() => {
    store.commit('toggleNotify', message)
    clearTimeout(timeout)
  },3000)
}

const changeTheme = (store, mode) => {
  store.commit('checkTheme', mode)
}

export default {
  showPopup,
  showNotify,
  changeTheme
}