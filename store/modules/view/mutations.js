const setPopupState = (state, type) => {
    state.popup.visible = true,
    state.popup.type = type
}

const toggleNotify = (state, message) => {
    state.notify.visible = !state.notify.visible
    state.notify.message = message
}

const togglePopupLoading = (state) => {
    state.popup.loading = !state.popup.loading
}

const checkTheme = (state, mode) => {
    state.theme = mode
}

const toggleVideoCall = (state) => {
    state.video.talk = !state.video.talk
}

const disableVideoCall = (state) => {
    state.video.talk = false
}

const toggleMobileUsers = (state) => {
    state.messages.mobileUsersOpened = !state.messages.mobileUsersOpened
}

export default {
    setPopupState,
    toggleNotify,
    togglePopupLoading,
    checkTheme,
    toggleVideoCall,
    disableVideoCall,
    toggleMobileUsers
}