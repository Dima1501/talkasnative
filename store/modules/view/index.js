import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
  popup: {
    visible: false,
    type: 'registration',
    loading: false
  },
  video: {
    call: false,
    talk: false
  },
  notify: {
    visible: false,
    message: ''
  },
  theme: 'light',
  messages: {
    mobileUsersOpened: false
  }
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
};