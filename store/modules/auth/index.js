import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
  authentificated: false,
  user: {},
  userName: '',
  userData: {},
  userRef: null
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
};