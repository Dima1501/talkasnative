import {db} from '../../../main'
import firebase from 'firebase/app'
require('firebase/auth')
require('firebase/database')
import 'firebase/firestore'

const checkUserStatus = ({commit, dispatch, rootState}) => {
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      let data = {
        true: true,
        userData: user,
      }

      rootState.auth.userRef = firebase.firestore().collection('users').where('uid', '==', user.uid)

      var amOnline = firebase.database().ref(".info/connected")
      var userRef = firebase.database().ref('users/' + user.uid)

      amOnline.on('value', function(snapshot) {
        if (snapshot.val()) {
          userRef.onDisconnect().set('offline')
          userRef.set('online')
        }
      })
      document.onIdle = function () {
        userRef.set('idle')
      }
      document.onAway = function () {
        userRef.set('away')
      }
      document.onBack = function (isIdle, isAway) {
        userRef.set('online')
      }

      commit('authStatus', data)
      commit('updateUserName', user.displayName)
      
      rootState.auth.userRef.get().then((querySnapshot) => {
        querySnapshot.forEach(function(doc) {
          commit('setUserData', doc.data())
          dispatch('cards/loadCards', {}, {root: true})
          dispatch('messages/loadMessages', user.uid, {root: true})
          dispatch('messages/watchMessages', rootState, {root: true})
          dispatch('videocall/waitVideoCall', {}, {root: true})
        })
      })

    } else {
      commit('authStatus', false, null) 
      dispatch('cards/loadCards', {}, {root: true})
      dispatch('videocall/waitVideoCall', {}, {root: true})
    }
  })
}

const signInWithEmailAndPassword = ({commit, dispatch, store}, signInData) => {
  commit('view/togglePopupLoading', null, {root:true})
  firebase.auth().signInWithEmailAndPassword(signInData.email, signInData.password)
    .then(() => {
      commit('view/togglePopupLoading', null, {root:true})
    }).catch(function(error) {
      commit('view/togglePopupLoading', null, {root:true})
      dispatch('view/showNotify', error.message, {root:true})
    })
}

const signOutFunc = (store) => {
  let userRef = firebase.database().ref('users/' + store.state.user.uid)
  userRef.set('offline')

  firebase.auth().signOut().then(function() {
    console.log('success log out')
  }).catch(function(error) {
    console.log(error)
  })
}

const registrationFunc = ({commit, dispatch, store} , registrationData) => {
  commit('view/togglePopupLoading', null, {root:true})

  firebase.auth().createUserWithEmailAndPassword(registrationData.email, registrationData.password)
    .then((user) => {
      commit('view/togglePopupLoading', null, {root:true})
      firebase.auth().currentUser.updateProfile({
        displayName: registrationData.username
      }).then(function() {
        firebase.firestore().collection('users').add({
          uid: user.user.uid,
          userName: user.user.displayName,
          timestamp: 0 - user.user.metadata.a,
          knowLangs: registrationData.nativeLangs,
          learnLangs: registrationData.learnLangs,
          conversations: [],
          status: {
            online: true,
            videoCall: false
          },
          lastOnlineTime: 0,
          userImage: '',
          online: true
        })
        commit('updateUserName', registrationData.username)
      }).catch(function(error) {
        console.log(error)
      })
    }).catch(function(error) {
    commit('view/togglePopupLoading', null, {root:true})
    dispatch('view/showNotify', error.message, {root:true})
  })
}

const resetPasswordFunc = ({dispatch, commit, state} , reset) => {
  commit('view/togglePopupLoading', null, {root:true})
  firebase.auth().sendPasswordResetEmail(reset.email).then(function() {
    let message = 'We have sent message to ' + reset.email
    dispatch('view/showNotify', message, {root:true})
    commit('view/togglePopupLoading', null, {root:true})
  }).catch(function(error) {
    dispatch('view/showNotify', error.message, {root:true})
  })
}

export default {
  checkUserStatus,
  signInWithEmailAndPassword,
  signOutFunc,
  registrationFunc,
  resetPasswordFunc
}