const authStatus = (state, data) => {
    state.authentificated = data.true
    state.user = data.userData
}

const updateUser = (state, user) => {
    state.user = user
}

const updateUserName = (state, name) => {
    state.userName = name
}

const setUserData = (state, data) => {
    state.userData = data
}

export default {
    authStatus,
    updateUser,
    updateUserName,
    setUserData
}