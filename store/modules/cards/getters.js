const getUsers = state => state.users;

export default {
    getUsers,
}