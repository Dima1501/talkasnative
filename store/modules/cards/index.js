import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    users: [],
    lastLoaded: -1000000000000000,
    loadLen: 8
};

export default {
  namespaced:   true,
  state,
  actions,
  mutations,
  getters
};