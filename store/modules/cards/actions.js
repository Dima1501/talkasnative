import {db} from '../../../main'
import firebase from 'firebase/app'
require('firebase/auth')
require('firebase/database')
import 'firebase/firestore'

const loadCards = ({dispatch, commit, state, rootState}) => {
    firebase.firestore()
    .collection("users")
    .orderBy("timestamp")
    .startAfter(state.lastLoaded + 1)
    .limit(200)
    .get()
    .then(function(querySnapshot) {

        let tmstmp = 0

        querySnapshot.forEach(function(doc) {
            
            if (tmstmp == 0) {tmstmp = doc.data().timestamp} else {
                if (doc.data().timestamp > tmstmp) {tmstmp = doc.data().timestamp}
            }

            commit('cards/addCards', doc.data(), {root: true})
            commit('cards/updateLoadedTimestamp', tmstmp, {root: true})
        });
    }).catch(function(error) {
        console.log("Error getting documents: ", error);
    });
}

export default {
    loadCards
}