const addCards = (state, user) => {
    state.users.push(user)
}

const updateLoadedTimestamp = (state, timestamp) => {
    state.lastLoaded = timestamp
}

export default {
    addCards,
    updateLoadedTimestamp
}