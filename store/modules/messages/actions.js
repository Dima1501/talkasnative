import {db} from '../../../main'
import firebase from 'firebase/app'
require('firebase/auth')
require('firebase/database')
import 'firebase/firestore'

const loadMessages = ({commit, rootState}, id) => {

    firebase.firestore().collectionGroup('messages').orderBy('timestamp').where('to', '==', rootState.auth.user.uid).onSnapshot(snapshot => {
        let counter = 0
        
        snapshot.forEach(doc => {
            if (doc.data().read == false) {
                counter += 1
            }
        })
        commit('updateUnreadMessages', counter)
    })

    firebase.firestore().collectionGroup('messages').orderBy('timestamp').where('fromTo', 'array-contains', id).get().then(snapshot => {
        snapshot.forEach(doc => {
            firebase.firestore().collection('conversations').doc(doc.ref.parent.parent.id).get().then((subCollectionSnapshot) => {
                let data = {
                    conversationId: doc.ref.parent.parent.id,
                    conversationMessage: doc.data(),
                    conversationData: subCollectionSnapshot.data()
                }
                commit('uploadConversations', data)
            });
        })
    })
}

const sendMessage = (state, data) => {
    const getRoomId = () => {
        if (data.message.fromTo[0] > data.message.fromTo[1]) {
            return data.message.fromTo[0] + data.message.fromTo[1]
        } else {
            return data.message.fromTo[1] + data.message.fromTo[0]
        }
    }

    if (data.info.user2.userName.length == 0) {
        firebase.firestore().collection('users').where('uid', '==', data.message.fromTo[1]).get().then(snapshot => {
            data.info.user2.userName = snapshot.docs[0].data().userName
            if (snapshot.docs[0].data().photoUrl) {
                data.info.user2.photoUrl = snapshot.docs[0].data().photoUrl
            }
            data.info.user2.uid = snapshot.docs[0].data().uid
        }).then(() => {
            firebase.firestore().collection('conversations').doc(getRoomId()).collection('messages').add(data.message)
            firebase.firestore().collection('conversations').doc(getRoomId()).set(data.info)
        }).catch(err => console.log(err))
    } else {
        firebase.firestore().collection('conversations').doc(getRoomId()).collection('messages').add(data.message)
        firebase.firestore().collection('conversations').doc(getRoomId()).set(data.info)
    }
}

const watchMessages = (store, data) => {
    let readyToWatch = false

    firebase.firestore().collectionGroup('messages').orderBy('timestamp').where('fromTo', 'array-contains', data.auth.user.uid).onSnapshot(snapshot => {
        if (readyToWatch) {
            snapshot.forEach(doc => {
                let data = {
                    roomId: doc.ref.parent.parent.id,
                    message: doc.data()
                }

                if (store.state.conversations == undefined || store.state.conversations[doc.ref.parent.parent.id] == undefined || store.state.conversations[doc.ref.parent.parent.id].messages.length == 0) {
                    firebase.firestore().collection('conversations').doc(doc.ref.parent.parent.id).get().then((subCollectionSnapshot) => {
                        let data = {
                            conversationId: doc.ref.parent.parent.id,
                            conversationMessage: doc.data(),
                            conversationData: subCollectionSnapshot.data()
                        }
                        store.commit('uploadConversations', data)
                    });
                } else {
                    store.commit('addMessages', data)
                }
            })
        }

        readyToWatch = true
        
    })
    
}

export default {
    sendMessage,
    loadMessages,
    watchMessages
}