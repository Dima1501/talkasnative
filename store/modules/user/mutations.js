const setLoadedUserData = (state, data) => state.user = data

export default {
    setLoadedUserData
}