import path from 'path'
import fs from 'fs'

module.exports = {
  server: {
    https: {
      key: fs.readFileSync('./server.key', 'utf8'),
      cert: fs.readFileSync('./certificate.crt', 'utf8')
    },
    port: 443, // default: 3000
    host: 'talkasnative.com' // default: localhost
  },

  modules: [
    [
      '@nuxtjs/yandex-metrika',
      {
        id: '57282940',
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
      }
    ],
  ],

  /*
  ** Headers of the page
  */
  head: {
    title: 'Talk as native)',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'Talk as native', name: 'Talk as native', content: 'Text and video chat with native speakers from all over the world!' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  plugins: [
    '~plugins/filters.js'
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            configFile: '.eslintrc.js'
          }
        })
      }
    }
  }
}

